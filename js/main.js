let newTasks = [];

let JSONnewTasks = localStorage.getItem("dsNewTasks");

if (JSONnewTasks) {
    newTasks = JSON.parse(JSONnewTasks);
    renderItem(newTasks);
}

// new tasks
let saveTasks = () => {
    var JSONnewTasks = JSON.stringify(newTasks);
    localStorage.setItem("dsNewTasks", JSONnewTasks);
};

function addItem() {
    let taskContent = document.getElementById("newTask").value;
    if (taskContent == "") return;
    let newTask = new ToDo(taskContent, false);

    newTasks.push(newTask);

    renderItem(newTasks);
    saveTasks();
    document.getElementById("newTask").value = "";
}
function removeTask(index) {
    newTasks.splice(index, 1);
    saveTasks();
    renderItem(newTasks);
}

// completed tasks
let completeTask = (index) => {
    newTasks[index].isCompleted = true;

    saveTasks();

    renderItem(newTasks);
};

// sort
document.getElementById("two").addEventListener("click", () => {
    newTasks = newTasks.sort((a, b) => {
        var textA = a.content.toUpperCase();
        var textB = b.content.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
    });
    renderItem(newTasks);
    saveTasks();
});
document.getElementById("three").addEventListener("click", () => {
    newTasks = newTasks.sort((a, b) => {
        var textA = a.content.toUpperCase();
        var textB = b.content.toUpperCase();
        return textA > textB ? -1 : textA < textB ? 1 : 0;
    });
    renderItem(newTasks);
    saveTasks();
});
